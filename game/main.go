// Package game holds the data structure that represents the cumulative data collected by this program
package game

import (
	"git.sr.ht/~hristoast/openmw-validator/config"
	"git.sr.ht/~hristoast/openmw-validator/datafiles"
	"git.sr.ht/~hristoast/openmw-validator/log"
)

// GameConfig holds information about the user's setup
type GameConfig struct {
	Content          map[string]datafiles.DataFile
	DataFiles        map[string]datafiles.DataFile
	DataPaths        map[string]datafiles.DataPath
	FallbackArchives map[string]datafiles.DataFile
	Groundcover      map[string]datafiles.DataFile

	BadContent           []string
	BadFallbackArchives  []string
	BadGroundcover       []string
	BadPaths             []string
	DuplicateContentFile []string
	DuplicatePaths       []string
	EmptyPaths           []string
	ReplacedPaths        []string

	*config.CfgData
	*config.RunConfig

	*log.Logger
}

func NewGameConfig() *GameConfig {
	return &GameConfig{
		Content:          map[string]datafiles.DataFile{},
		DataFiles:        map[string]datafiles.DataFile{},
		DataPaths:        map[string]datafiles.DataPath{},
		FallbackArchives: map[string]datafiles.DataFile{},
		Groundcover:      map[string]datafiles.DataFile{},

		BadPaths:             []string{},
		BadContent:           []string{},
		BadFallbackArchives:  []string{},
		BadGroundcover:       []string{},
		DuplicateContentFile: []string{},
		DuplicatePaths:       []string{},
		EmptyPaths:           []string{},
		ReplacedPaths:        []string{},

		CfgData: &config.CfgData{
			Content:          []string{},
			FallbackArchives: []string{},
			Groundcover:      []string{},
			Data:             []string{},
		},

		RunConfig: &config.RunConfig{
			CaseSensitive: false,
			Cfg:           "",
			CheckPath:     "",
			ExitNonZero:   false,
			Help:          false,
			Log:           "",
			NoReplace:     false,
			Quiet:         false,
			RootDrive:     "",
			Stdout:        false,
			Verbose:       false,
		},

		Logger: &log.Logger{},
	}
}

func (g *GameConfig) NoProblems() bool {
	return len(g.BadContent) == 0 && len(g.BadFallbackArchives) == 0 && len(g.BadGroundcover) == 0 && len(g.BadPaths) == 0 && len(g.DuplicatePaths) == 0 && len(g.EmptyPaths) == 0 && len(g.ReplacedPaths) == 0 && len(g.DuplicateContentFile) == 0
}
