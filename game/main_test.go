package game

import (
	"testing"
)

func TestGameConfig(t *testing.T) {
	g := NewGameConfig()
	t.Run("Game Config has no problems", func(t *testing.T) {
		if !g.NoProblems() {
			t.Error("expected true; got: false")
		}
	})
	g.BadContent = append(g.BadContent, "VERY BAD")
	t.Run("Game Config has problems", func(t *testing.T) {
		if g.NoProblems() {
			t.Error("expected false; got: true")
		}
	})
}
