package datafiles

import (
	"os"
	"path/filepath"
	"testing"
)

func TestDataFile(t *testing.T) {
	p := NewPath(1, "/test/path1")
	d := NewFile("test_file1", p)
	found := make(map[string]DataFile)

	if d.AlreadyFound(found, false) != nil {
		t.Error("expected: not found; got: found")
	}

	found[d.FileName()] = d
	if d.AlreadyFound(found, false) == nil {
		t.Error("expected: found; got: not found")
	}

	expected := "test_file1"
	if d.FileName() != expected {
		t.Errorf("expected: %s; got: %s", expected, d.FileName())
	}

	// expected := "/test/path1/test_file1"
	// if d.FullPath() != expected {
	// 	t.Errorf("expected: %s; got: %s", expected, d.FullPath())
	// }

	if d.IsContent() {
		t.Error("expected: false; got: true")
	}

	content1 := NewFile("test1.esm", p)
	if !content1.IsContent() {
		t.Error("expected: true; got: false")
	}

	content2 := NewFile("test1.esp", p)
	if !content2.IsContent() {
		t.Error("expected: true; got: false")
	}

	content3 := NewFile("test1.omwaddon", p)
	if !content3.IsContent() {
		t.Error("expected: true; got: false")
	}

	content4 := NewFile("test1.omwscripts", p)
	if !content4.IsContent() {
		t.Error("expected: true; got: false")
	}

	if d.IsFallbackArchive() {
		t.Error("expected: false; got: true")
	}

	fba1 := NewFile("test1.omwgame", p)
	if !fba1.IsFallbackArchive() {
		t.Error("expected: true; got: false")
	}

	fba2 := NewFile("test1.bsa", p)
	if !fba2.IsFallbackArchive() {
		t.Error("expected: true; got: false")
	}

	if d.Order() != 0 {
		t.Errorf("expected: 0; got: %d", d.Order())
	}

	d.SetOrder(34)
	if d.Order() != 34 {
		t.Errorf("expected: 34; got: %d", d.Order())
	}

	expected = p.FullPath()
	if d.Path().FullPath() != expected {
		t.Errorf("expected: %s; got: %s", expected, d.Path().FullPath())
	}
}

func TestDataFiles(t *testing.T) {
	p := NewPath(1, "/test/path1")
	d1 := NewFile("test_file1", p)
	d2 := NewFile("test_file2", p)
	d3 := NewFile("test_file3", p)

	d1.SetOrder(1)
	d2.SetOrder(2)
	d3.SetOrder(3)

	var dataFiles DataFiles
	dataFiles = append(dataFiles, d1)
	dataFiles = append(dataFiles, d2)
	dataFiles = append(dataFiles, d3)

	if dataFiles.Len() != 3 {
		t.Errorf("expected: 3; got: %d", dataFiles.Len())
	}

	if dataFiles.Less(0, 2) == false {
		t.Error("expected: true; got: false")
	}

	if dataFiles.Less(2, 0) == true {
		t.Error("expected: false; got: true")
	}

	expected := "test_file1"
	got := dataFiles[0].FileName()
	if got != expected {
		t.Errorf("expected: %s; got: %s", expected, got)
	}

	dataFiles.Swap(0, 2)
	expected = "test_file3"
	got = dataFiles[0].FileName()
	if got != expected {
		t.Errorf("expected: %s; got: %s", expected, got)
	}
}

func TestDataPath(t *testing.T) {
	p := NewPath(1, "/test/path1")
	found := make(map[string]DataPath)

	if p.AlreadyFound(found, false) != nil {
		t.Error("expected: not found; got: found")
	}

	found[p.FullPath()] = p
	if p.AlreadyFound(found, false) == nil {
		t.Error("expected: found; got: not found")
	}

	if len(p.Content(nil)) != 0 {
		t.Errorf("expected: 0; got: %d", len(p.Content(nil)))
	}

	d1 := NewFile("test_file1.omwaddon", p)
	p.Content(d1)
	if len(p.Content(nil)) != 1 {
		t.Errorf("expected: 1; got: %d", len(p.Content(nil)))
	}

	if p.Duplicate(false) != false {
		t.Errorf("expected: false; got: %v", p.Duplicate(false))
	}

	p.Duplicate(true)
	if p.Duplicate(false) != true {
		t.Errorf("expected: true; got: %v", p.Duplicate(false))
	}

	if p.Exists() {
		t.Error("expected: false; got: true")
	}

	d, err := os.Getwd()
	if err != nil {
		t.Error(err.Error())
	}
	p2 := NewPath(2, filepath.Join(d, "..", "test-data", "path1"))
	if !p2.Exists() {
		t.Error("expected: true; got: false")
	}

	if len(p.FallbackArchives(nil)) != 0 {
		t.Errorf("expected: 0; got: %d", len(p.FallbackArchives(nil)))
	}

	d2 := NewFile("test_file1.omwgame", p)
	p.FallbackArchives(d2)
	if len(p.FallbackArchives(nil)) != 1 {
		t.Errorf("expected: 1; got: %d", len(p.FallbackArchives(nil)))
	}

	if len(p.DataFiles(nil)) != 0 {
		t.Errorf("expected: 0; got: %d", len(p.DataFiles(nil)))
	}

	p.DataFiles(d1)
	if len(p.DataFiles(nil)) != 1 {
		t.Errorf("expected: 1; got: %d", len(p.DataFiles(nil)))
	}

	expected := "/test/path1"
	if p.FullPath() != expected {
		t.Errorf("expected: %s; got: %s", expected, p.FullPath())
	}

	if p.Order() != 1 {
		t.Errorf("expected: 1; got: %d", p.Order())
	}

	if p.Order() != 1 {
		t.Errorf("expected: 1; got: %d", p.Order())
	}

	if len(p.Replaced(nil)) != 0 {
		t.Errorf("expected: 0; got: %d", p.Replaced(nil))
	}

	p.Replaced(d1)
	if len(p.Replaced(nil)) != 1 {
		t.Errorf("expected: 1; got: %d", p.Replaced(nil))
	}
}

func TestDataPaths(t *testing.T) {
	p1 := NewPath(1, "/test/path1")
	p2 := NewPath(2, "/test/path2")
	p3 := NewPath(3, "/test/path3")

	var dataPaths DataPaths
	dataPaths = append(dataPaths, p1)
	dataPaths = append(dataPaths, p2)
	dataPaths = append(dataPaths, p3)

	if dataPaths.Len() != 3 {
		t.Errorf("expected: 3; got: %d", dataPaths.Len())
	}

	if dataPaths.Less(0, 2) == false {
		t.Error("expected: true; got: false")
	}

	if dataPaths.Less(2, 0) == true {
		t.Error("expected: false; got: true")
	}

	expected := "/test/path1"
	got := dataPaths[0].FullPath()
	if got != expected {
		t.Errorf("expected: %s; got: %s", expected, got)
	}

	dataPaths.Swap(0, 2)
	expected = "/test/path3"
	got = dataPaths[0].FullPath()
	if got != expected {
		t.Errorf("expected: %s; got: %s", expected, got)
	}
}
