// Package datafiles contains code for handing data, content, and groundcover files.
package datafiles

import (
	"os"
	"strings"
)

// DataFile describes a game data file such as a mesh, plugin, texture, or other.
type DataFile interface {
	AlreadyFound(map[string]DataFile, bool) DataFile
	FileName() string
	FullPath() string
	IsContent() bool
	IsFallbackArchive() bool
	Order() int
	Path() *Path
	SetOrder(int)
}

// DataPath describes a configured data path which should contain some data files.
type DataPath interface {
	AlreadyFound(map[string]DataPath, bool) DataPath
	Content(DataFile) []DataFile
	Duplicate(bool) bool
	Exists() bool
	FallbackArchives(DataFile) []DataFile
	DataFiles(DataFile) []DataFile
	FullPath() string
	FullyReplaced() bool
	Order() int
	Replaced(DataFile) []DataFile
	TotalFiles() int
}

// DataPaths implements sort.Interface
type DataPaths []DataPath

func (d DataPaths) Len() int           { return len(d) }
func (d DataPaths) Less(i, j int) bool { return d[i].Order() < d[j].Order() }
func (d DataPaths) Swap(i, j int)      { d[i], d[j] = d[j], d[i] }

// DataFiles implements sort.Interface
type DataFiles []DataFile

func (df DataFiles) Len() int           { return len(df) }
func (df DataFiles) Less(i, j int) bool { return df[i].Order() < df[j].Order() }
func (df DataFiles) Swap(i, j int)      { df[i], df[j] = df[j], df[i] }

// File represents a game data file.
type File struct {
	file  string
	order int
	path  *Path
}

// Path represents a configured data path.
type Path struct {
	dataFiles        []DataFile
	content          []DataFile
	duplicate        bool
	fallbackArchives []DataFile
	order            int
	path             string
	replaced         []DataFile
}

// NewFile constructs a new File pointer.
func NewFile(file string, path *Path) *File {
	return &File{
		file:  file,
		order: 0,
		path:  path,
	}
}

// NewPath constructs a new Path pointer.
func NewPath(order int, path string) *Path {
	return &Path{
		dataFiles:        []DataFile{},
		content:          []DataFile{},
		duplicate:        false,
		fallbackArchives: []DataFile{},
		order:            order,
		path:             path,
		replaced:         []DataFile{},
	}
}

// AlreadyFound indicates if a File has already been registered as a game data file.
func (f *File) AlreadyFound(found map[string]DataFile, caseSensitive bool) DataFile {
	var fileName string
	if caseSensitive {
		fileName = f.VfsPath()
	} else {
		// Coerce the file name to all lowercase if we don't care about case sensitivity
		fileName = strings.ToLower(f.VfsPath())
	}
	if replaced, ok := found[fileName]; ok {
		return replaced
	}
	return nil
}

// IsContent indicates if this File is a plugin of some sort.
func (f *File) IsContent() bool {
	file := strings.ToLower(f.file)
	return strings.HasSuffix(file, ".esm") || strings.HasSuffix(file, ".esp") || strings.HasSuffix(file, ".omwaddon") || strings.HasSuffix(file, ".omwscripts")
}

func (f *File) IsFallbackArchive() bool {
	file := strings.ToLower(f.file)
	return strings.HasSuffix(file, ".bsa") || strings.HasSuffix(file, ".omwgame")
}

func (f *File) Order() int {
	return f.order
}

func (f *File) SetOrder(o int) { f.order = o }

// FullPath gives the full path to a File.
func (f *File) FullPath() string {
	return f.file
}

// FileName gives the File path relative to the data path root (e.g. meshes/x/foo.nif).
func (f *File) FileName() string {
	return f.file
}

func (f *File) Path() *Path {
	return f.path
}

func (f *File) VfsPath() string {
	// I hate computers
	dataPath := strings.ReplaceAll(f.path.path, "\\", "/")
	vfsPath := strings.ReplaceAll(f.file, "\\", "/")
	return strings.ReplaceAll(vfsPath, dataPath, "")
}

// AlreadyFound indicates if a Path has already been registered as a data path.
func (p *Path) AlreadyFound(found map[string]DataPath, caseSensitive bool) DataPath {
	var pathName string
	if caseSensitive {
		pathName = p.FullPath()
	} else {
		// Coerce the path name to all lowercase if we don't care about case sensitivity
		pathName = strings.ToLower(p.FullPath())
	}
	if _, ok := found[pathName]; ok {
		return p
	}
	return nil
}

// Exists indicates if the given Path exists and is actually a directory.
func (p *Path) Exists() bool {
	stat, err := os.Stat(p.path)
	if stat != nil {
		if stat.IsDir() && err == nil {
			return true
		}
	}
	return false
}

// FallbackArchives is the total number of files in a given DataPath
func (p *Path) FallbackArchives(f DataFile) []DataFile {
	if f != nil {
		p.fallbackArchives = append(p.fallbackArchives, f)
	}
	return p.fallbackArchives
}

// DataFiles is the total number of files in a given DataPath
func (p *Path) DataFiles(f DataFile) []DataFile {
	if f != nil {
		p.dataFiles = append(p.dataFiles, f)
	}
	return p.dataFiles
}

// Content is the total number of files in a given DataPath
func (p *Path) Content(f DataFile) []DataFile {
	if f != nil {
		p.content = append(p.content, f)
	}
	return p.content
}

// Duplicate is the total number of files in a given DataPath
func (p *Path) Duplicate(b bool) bool {
	if b {
		p.duplicate = true
		return false
	}
	return p.duplicate
}

// FullPath is a helper for accessing a Path's string representation.
func (p *Path) FullPath() string {
	return p.path
}

func (p *Path) Order() int {
	return p.order
}

func (p *Path) Replaced(d DataFile) []DataFile {
	if d != nil {
		p.replaced = append(p.replaced, d)
	}
	return p.replaced
}

func (p *Path) TotalFiles() int {
	return len(p.dataFiles) + len(p.content) + len(p.fallbackArchives)
}

func (p *Path) FullyReplaced() bool {
	return p.TotalFiles() == len(p.Replaced(nil))
}
