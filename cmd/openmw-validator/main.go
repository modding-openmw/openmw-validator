package main

import (
	"os"
	"runtime"

	"git.sr.ht/~hristoast/openmw-validator/cli"
	"git.sr.ht/~hristoast/openmw-validator/log"
	"git.sr.ht/~hristoast/openmw-validator/validations"
)

// Entry point for the cli application
func main() {
	// Get the user's OS and parse CLI args
	userOS := runtime.GOOS
	gameCfg, err := cli.ParseArgs(userOS)
	if err != nil {
		log.EmitError(err.Error())
		os.Exit(1)
	}

	// Set log output as needed
	var out string
	if gameCfg.Stdout {
		if userOS == "windows" {
			out = gameCfg.Log
			gameCfg.Stdout = false
		} else {
			out = os.Stdout.Name()
		}
	} else {
		out = gameCfg.Log
	}
	file, err := os.OpenFile(out, os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
	defer file.Close()
	if err != nil {
		log.EmitError(err.Error())
		os.Exit(1)
	}
	gameCfg.Logger = log.New(file, gameCfg.Quiet, gameCfg.Verbose)

	// Greet the user and run validations
	gameCfg.Greeting(gameCfg.CaseSensitive, gameCfg.Cfg)
	validatedCfg, start, err := validations.Run(gameCfg)
	if err != nil {
		gameCfg.EmitError(err.Error())
		os.Exit(1)
	}

	// Give a summary of all validations
	problems := validations.Summary(validatedCfg, start)
	if problems && validatedCfg.ExitNonZero {
		os.Exit(1)
	}
}
