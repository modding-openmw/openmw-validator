## OpenMW-Validator Changelog

#### 1.14

* The summary will now print to the console on Windows

#### 1.13

* Only print the summary by default, show all info by passing `--verbose`

#### 1.12

* Build against 1.23.0 (up from 1.17.1)
* Fixed a logging bug that caused some file paths to be printed incorrectly.

#### 1.11

* Windows folder paths are properly detected (special thanks to AnyOldName3!)

#### 1.10

* The program now exits appropriately when an error happens

#### 1.9

* Fixed printing the wrong thing when something gets replaced (the file that did the replacing versus the one that was replaced)

#### 1.8

* Fixed a seemingly Linux-only problem with files in deeply nested subdirs
