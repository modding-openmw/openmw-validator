// Package config ...
package config

// Version is intended to be set at link-time (see the project Makefile)
var Version string = "0.0"

type CfgData struct {
	Content          []string
	FallbackArchives []string
	Groundcover      []string
	Data             []string
}

type RunConfig struct {
	CaseSensitive bool
	Cfg           string
	CheckPath     string
	ExitNonZero   bool
	Help          bool
	Log           string
	NoReplace     bool
	Quiet         bool
	RootDrive     string
	Stdout        bool
	Verbose       bool
}
