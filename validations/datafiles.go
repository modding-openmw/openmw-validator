package validations

import (
	"fmt"
	"path/filepath"
	"strings"

	"git.sr.ht/~hristoast/openmw-validator/config"
	"git.sr.ht/~hristoast/openmw-validator/datafiles"
	"git.sr.ht/~hristoast/openmw-validator/game"
)

// Specific handling for content files - check configured files against what was found.
func contentFiles(cfgData *config.CfgData, gameCfg *game.GameConfig) *game.GameConfig {
	gameCfg.Emit("")
	gameCfg.Emit("Checking all content files...")
	gameCfg.Emit("")
	configured := make(map[string]bool)
	num := 1
	for _, c := range cfgData.Content {
		content := sanitizePath(c, "content=")
		var ct string
		if gameCfg.CaseSensitive {
			ct = content
		} else {
			ct = strings.ToLower(content)
		}

		if _, ok := configured[ct]; ok {
			gameCfg.DuplicateContentFile = append(gameCfg.DuplicateContentFile, content)
			gameCfg.Emit(fmt.Sprintf("WARNING: Duplicate content file is configured: %s", content))
		} else {
			if alreadyFound(gameCfg.Content, ct) {
				gameCfg.Emit(fmt.Sprintf("Checking #%d: %s", num, content))
				gameCfg.Content[ct].SetOrder(num)
				num += 1
			} else {
				gameCfg.BadContent = append(gameCfg.BadContent, content)
				gameCfg.Emit(fmt.Sprintf("WARNING: Non-existent content file is configured: %s", content))
			}
		}
		configured[ct] = true
	}
	return gameCfg
}

// Specific handling for "fallback archives"
func fallbackArchives(cfgData *config.CfgData, gameCfg *game.GameConfig) *game.GameConfig {
	gameCfg.Emit("")
	gameCfg.Emit("Checking all fallback archives...")
	gameCfg.Emit("")
	num := 1
	for _, f := range cfgData.FallbackArchives {
		fa := sanitizePath(f, "fallback-archive=")
		var fba string
		if gameCfg.CaseSensitive {
			fba = fa
		} else {
			fba = strings.ToLower(fa)
		}
		if alreadyFound(gameCfg.FallbackArchives, fba) {
			gameCfg.Emit(fmt.Sprintf("Checking #%d: %s", num, fa))
			gameCfg.FallbackArchives[fba].SetOrder(num)
			num += 1
		} else {
			gameCfg.BadFallbackArchives = append(gameCfg.BadFallbackArchives, fa)
			gameCfg.Emit(fmt.Sprintf("WARNING: Non-existent fallback archive is configured: %s", fa))
		}
	}
	return gameCfg
}

// Specific handling for groundcover files
func groundcoverFiles(cfgData *config.CfgData, gameCfg *game.GameConfig) *game.GameConfig {
	gameCfg.Emit("")
	gameCfg.Emit("Checking all groundcover files...")
	gameCfg.Emit("")
	num := 1
	bunk := 0
	for _, g := range cfgData.Groundcover {
		gc := sanitizePath(g, "groundcover=")
		var gcf string
		if gameCfg.CaseSensitive {
			gcf = gc
		} else {
			gcf = strings.ToLower(gc)
		}
		if alreadyFound(gameCfg.Content, gcf) {
			gameCfg.Emit(fmt.Sprintf("Checking #%d: %s", num, gc))
			gameCfg.Groundcover[gcf] = gameCfg.Content[gcf]
			gameCfg.Groundcover[gcf].SetOrder(num)
			delete(gameCfg.Content, gcf)
			num += 1
		} else {
			gameCfg.BadGroundcover = append(gameCfg.BadGroundcover, gc)
			gameCfg.Emit(fmt.Sprintf("WARNING: Non-existent groundcover file is configured: %s", gc))
			bunk += 1
		}
	}
	if num == 1 && bunk == 0 {
		gameCfg.Emit("No groundcover files configured")
	}
	return gameCfg
}

// Handle found data files in one of three ways: content file, fallback
// archive, everything else. Also update a given fileCount along the way
func handleFile(df string, path *datafiles.Path, g *game.GameConfig, fileCount, pathNum int) int {
	file := datafiles.NewFile(df, path)
	if file.IsContent() {

		// Don't count content files found in subdirs, just give a warning about it
		if strings.Contains(file.FileName(), string(filepath.Separator)) {
			splitup := strings.Split(file.FileName(), string(filepath.Separator))
			if len(splitup) > 1 {
				g.Emit(fmt.Sprintf("WARNING: Content file found in subdir: %s", file.FullPath()))
				return fileCount
			}
		}

		var contentFile string
		if g.CaseSensitive {
			contentFile = filepath.Base(file.FileName())
		} else {
			contentFile = strings.ToLower(filepath.Base(file.FileName()))
		}
		if pathNum > 1 {
			if replaced, ok := g.Content[contentFile]; ok {
				if !g.NoReplace {
					g.Emit(fmt.Sprintf("Replaced: %s", replaced.FullPath()))
				}
				_ = replaced.Path().Replaced(replaced)
			}
		}
		g.Content[contentFile] = file
		path.Content(file)

	} else if file.IsFallbackArchive() {
		// Don't count fallback archives found in subdirs, just give a warning about it
		if strings.Contains(file.FileName(), string(filepath.Separator)) {
			splitup := strings.Split(file.FileName(), string(filepath.Separator))
			if len(splitup) > 1 {
				g.Emit(fmt.Sprintf("WARNING: Fallback archive found in subdir: %s", file.FullPath()))
				return fileCount
			}
		}

		faFile := filepath.Base(file.FileName())
		if pathNum > 1 {
			if replaced, ok := g.FallbackArchives[faFile]; ok {
				if !g.NoReplace {
					g.Emit(fmt.Sprintf("Replaced: %s", replaced.FullPath()))
				}
				_ = replaced.Path().Replaced(replaced)
			}
		}
		if g.CaseSensitive {
			faFile = file.FileName()
		} else {
			faFile = strings.ToLower(file.FileName())
		}
		g.FallbackArchives[faFile] = file
		path.FallbackArchives(file)

	} else {
		if pathNum > 1 {
			replaced := file.AlreadyFound(g.DataFiles, g.CaseSensitive)
			if replaced != nil {
				if !g.NoReplace {
					g.Emit(fmt.Sprintf("Replaced: %s", replaced.FullPath()))
				}
				_ = replaced.Path().Replaced(replaced)
			}
		}
		if g.CaseSensitive {
			g.DataFiles[file.VfsPath()] = file
		} else {
			g.DataFiles[strings.ToLower(file.VfsPath())] = file
		}
		path.DataFiles(file)
	}

	fileCount += 1
	return fileCount
}
