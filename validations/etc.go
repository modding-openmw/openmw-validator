package validations

import (
	"fmt"
	"path/filepath"
	"strings"
	"time"

	"git.sr.ht/~hristoast/openmw-validator/datafiles"
	"git.sr.ht/~hristoast/openmw-validator/game"
)

func alreadyFound(in map[string]datafiles.DataFile, file string) bool {
	_, found := in[file]
	return found
}

// Check a file path against an internal map of files that should be ignored and
// return a bool indicating if it is or is not
func ignored(fullPath string) bool {
	ok := false

	// First check for an ignored extension.
	ignoredExtensions := []string{
		".7z",
		".txt",
		".zip",
	}
	for _, ext := range ignoredExtensions {
		if strings.HasSuffix(fullPath, ext) {
			return true
		}
	}

	// Then check for a specific file name.
	ignoredFiles := map[string]bool{
		".ds_store":     true,
		".gitkeep":      true,
		".gitignore":    true,
		"license":       true,
		"changelog":     true,
		"changelog.md":  true,
		"changelog.txt": true,
		"thumbs.db":     true,
		"readme":        true,
		"readme.md":     true,
		"readme.txt":    true,
	}
	if _, ok = ignoredFiles[strings.ToLower(filepath.Base(fullPath))]; ok {
		return ok // true
	}

	// Don't check git dirs
	if strings.Contains(fullPath, ".git") {
		return true
	}

	return ok // false
}

func elapsed(start time.Time, g *game.GameConfig) {
	end := time.Now()

	msPerSec := 1000
	secPerMin := 60
	elapsed := end.Sub(start)
	totalMs := int(elapsed.Milliseconds())
	totalSecs := totalMs / msPerSec
	totalMins := totalSecs / secPerMin
	leftoverMs := totalMs - totalSecs*msPerSec
	leftoverSecs := totalSecs - totalMins*secPerMin

	minuteWord := "minute"
	secondWord := "second"
	millisWord := "millisecond"

	if totalMins == 0 || totalMins > 1 {
		minuteWord += "s"
	}
	if leftoverSecs == 0 || leftoverSecs > 1 {
		secondWord += "s"
	}
	if leftoverMs == 0 || leftoverMs > 1 {
		millisWord += "s"
	}

	g.Emit(fmt.Sprintf("Elapsed: %d %s, %d %s, %d %s", totalMins, minuteWord, leftoverSecs, secondWord, leftoverMs, millisWord))
}

func percent(i, j int) float64 { return float64(i) / float64(j) * 100 }

// Get just the path from a string read directly from an openmw.cfg file,
// hopefully in a way that doesn't upset all the OSes
func sanitizePath(path string, prefix string) string {
	p := strings.Replace(path, prefix, "", -1)
	p = strings.TrimPrefix(p, "'")
	p = strings.TrimPrefix(p, "\"")
	p = strings.TrimSuffix(p, "\r")
	p = strings.TrimSuffix(p, "'")
	p = strings.TrimSuffix(p, "\"")
	return p
}
