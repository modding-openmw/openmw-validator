// Package validations is where all of the checking and verifying happens.
package validations

import (
	"fmt"
	"sort"
	"time"

	"git.sr.ht/~hristoast/openmw-validator/datafiles"
	"git.sr.ht/~hristoast/openmw-validator/files"
	"git.sr.ht/~hristoast/openmw-validator/game"
	"git.sr.ht/~hristoast/openmw-validator/log"
)

// Summary prints a summary of the validation.
func Summary(validatedCfg *game.GameConfig, start time.Time) bool {
	if !validatedCfg.Stdout {
		fmt.Printf("Validation completed, log written to: %s\n", validatedCfg.Log)
	}
	log.Emit("")
	log.Emit("======== Validation summary:")
	validatedCfg.Emit("")

	validatedCfg.Emit("Reviewing all data paths...")
	validatedCfg.Emit("")

	var dataPaths datafiles.DataPaths
	for _, dp := range validatedCfg.DataPaths {
		dataPaths = append(dataPaths, dp)
	}
	sort.Sort(dataPaths)

	var totalReplaced datafiles.DataFiles
	for _, p := range dataPaths {
		fileWord := "data file"
		fallbackArchiveWord := "fallback archive"
		contentWord := "content file"
		contentCount := len(p.Content(nil))
		fallbackArchivesCount := len(p.FallbackArchives(nil))
		fileCount := len(p.DataFiles(nil))
		if contentCount > 1 || contentCount == 0 {
			contentWord += "s"
		}
		if fallbackArchivesCount > 1 || fallbackArchivesCount == 0 {
			fallbackArchiveWord += "s"
		}
		if fileCount > 1 || fileCount == 0 {
			fileWord += "s"
		}
		if fileCount == 0 && contentCount == 0 && fallbackArchivesCount == 0 {
			validatedCfg.Emit(fmt.Sprintf("Data path #%d \"%s\" has no data files!", p.Order(), p.FullPath()))

		} else {
			fWord := "file"
			wWord := "was"
			replaced := len(p.Replaced(nil))
			if replaced == 0 || replaced > 1 {
				fWord += "s"
				wWord = "were"
			}
			s := "Data path #%d \"%s\" has %d %s, %d %s, and %d %s. %d %s %s replaced (%.f%%)"
			if p.Duplicate(false) {
				s += " (this is a duplicate path)"
			}
			validatedCfg.Emit(fmt.Sprintf(s, p.Order(), p.FullPath(), fileCount, fileWord, contentCount, contentWord, fallbackArchivesCount, fallbackArchiveWord, replaced, fWord, wWord, percent(replaced, fileCount+contentCount+fallbackArchivesCount)))
			for _, r := range p.Replaced(nil) {
				totalReplaced = append(totalReplaced, r)
			}
			if p.FullyReplaced() {
				validatedCfg.ReplacedPaths = append(validatedCfg.ReplacedPaths, p.FullPath())
			}
		}
	}

	validatedCfg.Emit("")
	validatedCfg.Emit("Reviewing fallback archives...")
	validatedCfg.Emit("")
	var fallbackArchives datafiles.DataFiles
	for _, content := range validatedCfg.FallbackArchives {
		fallbackArchives = append(fallbackArchives, content)
	}
	sort.Sort(fallbackArchives)
	for _, content := range fallbackArchives {
		validatedCfg.Emit(fmt.Sprintf("#%d: %s", content.Order(), content.FullPath()))
	}

	validatedCfg.Emit("")
	validatedCfg.Emit("Reviewing content files...")
	validatedCfg.Emit("")
	var contentFiles datafiles.DataFiles
	for _, content := range validatedCfg.Content {
		if content.Order() > 0 {
			contentFiles = append(contentFiles, content)
		}
	}
	sort.Sort(contentFiles)
	for _, content := range contentFiles {
		validatedCfg.Emit(fmt.Sprintf("#%d: %s", content.Order(), content.FullPath()))
	}

	if len(validatedCfg.Groundcover) > 0 {
		validatedCfg.Emit("")
		validatedCfg.Emit("Reviewing groundcover files...")
		validatedCfg.Emit("")
		var groundcoverFiles datafiles.DataFiles
		for _, content := range validatedCfg.Groundcover {
			groundcoverFiles = append(groundcoverFiles, content)
		}
		sort.Sort(groundcoverFiles)
		for _, content := range groundcoverFiles {
			validatedCfg.Emit(fmt.Sprintf("#%d: %s", content.Order(), content.FullPath()))
		}
	}

	log.Emit("")
	log.Emit(fmt.Sprintf("Total data files found: %d", len(validatedCfg.DataFiles)))
	log.Emit(fmt.Sprintf("Total fallback archives found: %d", len(validatedCfg.FallbackArchives)))
	log.Emit(fmt.Sprintf("Total content files found: %d", len(validatedCfg.Content)))
	log.Emit(fmt.Sprintf("Total groundcover files found: %d", len(validatedCfg.Groundcover)))
	log.Emit(fmt.Sprintf("Total Replaced file count: %d (%.f%%)", len(totalReplaced), percent(len(totalReplaced), len(validatedCfg.DataFiles)+len(validatedCfg.FallbackArchives)+len(validatedCfg.Content)+len(validatedCfg.Groundcover))))
	log.Emit("")

	problems := false
	if validatedCfg.NoProblems() {
		log.Emit("Great Job! No problems detected.")
		log.Emit("")
	} else {
		problems = true
		log.Emit("======== Some problems were detected:")
		log.Emit("")

		if len(validatedCfg.BadPaths) > 0 {
			log.Emit("Bad paths were configured:")
			log.Emit("")
			for _, p := range validatedCfg.BadPaths {
				log.Emit(p)
			}
			log.Emit("")
		}

		if len(validatedCfg.BadContent) > 0 {
			log.Emit("Bad content files were configured:")
			log.Emit("")
			for _, c := range validatedCfg.BadContent {
				log.Emit(c)
			}
			log.Emit("")
		}

		if len(validatedCfg.BadFallbackArchives) > 0 {
			log.Emit("Bad fallback archives were configured:")
			log.Emit("")
			for _, c := range validatedCfg.BadFallbackArchives {
				log.Emit(c)
			}
			log.Emit("")
		}

		if len(validatedCfg.BadGroundcover) > 0 {
			log.Emit("Bad groundcover files were configured:")
			log.Emit("")
			for _, c := range validatedCfg.BadGroundcover {
				log.Emit(c)
			}
			log.Emit("")
		}

		if len(validatedCfg.DuplicateContentFile) > 0 {
			log.Emit("Duplicate content files were configured:")
			log.Emit("")
			for _, c := range validatedCfg.DuplicateContentFile {
				log.Emit(c)
			}
			log.Emit("")
		}

		if len(validatedCfg.DuplicatePaths) > 0 {
			log.Emit("Duplicate paths were configured:")
			log.Emit("")
			for _, c := range validatedCfg.DuplicatePaths {
				log.Emit(c)
			}
			log.Emit("")
		}

		if len(validatedCfg.EmptyPaths) > 0 {
			log.Emit("Empty paths were configured:")
			log.Emit("")
			for _, c := range validatedCfg.EmptyPaths {
				log.Emit(c)
			}
			log.Emit("")
		}

		if len(validatedCfg.ReplacedPaths) > 0 {
			log.Emit("Fully replaced paths were configured:")
			log.Emit("")
			for _, c := range validatedCfg.ReplacedPaths {
				log.Emit(c)
			}
			log.Emit("")
		}
	}

	if validatedCfg.CheckPath != "" {
		validatedCfg.Emit("======== Reviewing path:")
		if path, ok := validatedCfg.DataPaths[validatedCfg.CheckPath]; ok {
			fileCount := len(path.DataFiles(nil))
			fbaCount := len(path.FallbackArchives(nil))
			contentCount := len(path.Content(nil))
			replacedCount := len(path.Replaced(nil))
			totalFiles := fileCount + fbaCount + contentCount
			validatedCfg.Emit("")
			validatedCfg.Emit(validatedCfg.CheckPath)
			validatedCfg.Emit("")
			validatedCfg.Emit(fmt.Sprintf("Data file count: %d (%.f%%)", fileCount, percent(fileCount, totalFiles)))
			validatedCfg.Emit(fmt.Sprintf("Fallback archive count: %d (%.f%%)", fbaCount, percent(fbaCount, totalFiles)))
			validatedCfg.Emit(fmt.Sprintf("Content file count: %d (%.f%%)", contentCount, percent(contentCount, totalFiles)))
			validatedCfg.Emit(fmt.Sprintf("Replaced count: %d (%.f%%)", replacedCount, percent(replacedCount, totalFiles)))
			if totalFiles == replacedCount {
				validatedCfg.Emit("")
				validatedCfg.Emit("This path is fully replaced!")
			}
			validatedCfg.Emit("")
		} else {
			validatedCfg.Emit("")
			validatedCfg.Emit(fmt.Sprintf("Not a configured path: %s", validatedCfg.CheckPath))
			validatedCfg.Emit("")
		}
	}

	log.Emit("Validation completed!")
	elapsed(start, validatedCfg)
	return problems
}

// Run examines the configuration as read from the provided openmw.cfg
// file and perform various checks and validations on them
func Run(gameCfg *game.GameConfig) (*game.GameConfig, time.Time, error) {
	start := time.Now()

	cfgData, err := files.ReadCfg(gameCfg.Cfg)
	if err != nil {
		return gameCfg, start, err
	}
	gameCfg, err = dataPaths(cfgData, gameCfg)
	if err != nil {
		return gameCfg, start, err
	}
	gameCfg = fallbackArchives(cfgData, gameCfg)
	gameCfg = contentFiles(cfgData, gameCfg)
	gameCfg = groundcoverFiles(cfgData, gameCfg)
	return gameCfg, start, nil
}
