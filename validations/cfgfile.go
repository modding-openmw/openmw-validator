package validations

import (
	"os"
)

// CfgExists determines if the given cfg file exists and is actually a readable file
func CfgExists(path string) bool {
	stat, err := os.Stat(path)
	if stat != nil {
		if !stat.IsDir() && err == nil {
			return true
		}
	}
	return false
}
