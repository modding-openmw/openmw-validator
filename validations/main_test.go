package validations

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"git.sr.ht/~hristoast/openmw-validator/game"
	"git.sr.ht/~hristoast/openmw-validator/log"
)

func TestCfgFileExists(t *testing.T) {
	var d string
	var err error
	d, err = os.Getwd()
	if err != nil {
		t.Error(err.Error())
	}
	srcDir := filepath.Join(d, "..")
	doesExist := filepath.Join(srcDir, "problems-openmw.cfg")
	doesntExist := filepath.Join(srcDir, "fake-openmw.cfg")

	expected := true
	exists := CfgExists(doesExist)
	if !exists {
		t.Errorf("expected: %v; got: %v", expected, exists)
	}

	expected = false
	exists = CfgExists(doesntExist)
	if exists {
		t.Errorf("expected: %v; got: %v", expected, exists)
	}
}

func TestValidationsProblems(t *testing.T) {
	gc := game.NewGameConfig()
	var start time.Time
	var err error
	var d string
	tempDir := t.TempDir()
	out := filepath.Join(tempDir, "test.log")
	gc.Log = out
	d, err = os.Getwd()
	if err != nil {
		t.Error(err.Error())
	}
	srcDir := filepath.Join(d, "..")
	problemCfg := filepath.Join(srcDir, "problems-openmw.cfg")
	gc.Cfg = problemCfg
	file, err := os.OpenFile(out, os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		t.Error(err.Error())
	}
	defer file.Close()
	gc.Logger = log.New(file, false, true)
	gc.Greeting(false, gc.Cfg)
	gc, start, err = Run(gc)
	if err != nil {
		t.Error(err.Error())
	}
	Summary(gc, start)
	logOut, err := os.Open(out)
	if err != nil {
		t.Error(err.Error())
	}
	b, err := io.ReadAll(logOut)
	if err != nil {
		t.Error(err.Error())
	}
	s := string(b)

	actualReplacedCount := 0
	for _, p := range gc.DataPaths {
		actualReplacedCount += len(p.Replaced(nil))
	}

	expected := " ======== Welcome to openmw-validator"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = " ======== Performing validation:"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = fmt.Sprintf("WARNING: Empty path configured: %s/test-data/path3", srcDir)
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = fmt.Sprintf("WARNING: Bad path found: %s/test-data/path4", srcDir)
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = "WARNING: Duplicate content file is configured: Test1.omwaddon"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = fmt.Sprintf("WARNING: Duplicate path found: %s/test-data/path2", srcDir)
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = "WARNING: Non-existent fallback archive is configured: Test3.bsa"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = "WARNING: Non-existent content file is configured: Test2.esp"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = "WARNING: Non-existent groundcover file is configured: Groundcover3.omwaddon"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	// expected = fmt.Sprintf("Data path #1 \"%s/test-data/path1\" has 42 data files, 6 content files, and 2 fallback archives. 6 files were replaced (12%%)", srcDir)
	// if !strings.Contains(s, expected) {
	// 	t.Errorf("expected: %s", expected)
	// }

	expected = fmt.Sprintf("Data path #2 \"%s/test-data/path2\" has 42 data files, 6 content files, and 2 fallback archives. 48 files were replaced (96%%)", srcDir)
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = fmt.Sprintf("Data path #3 \"%s/test-data/path3\" has no data files!", srcDir)
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = fmt.Sprintf("Data path #4 \"%s/test-data/path2\" has 42 data files, 6 content files, and 2 fallback archives. 1 file was replaced (2%%) (this is a duplicate path)", srcDir)
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = fmt.Sprintf("Data path #5 \"%s/test-data/path5\" has 0 data files, 1 content file, and 0 fallback archives. 1 file was replaced (100%%)", srcDir)
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = fmt.Sprintf("Data path #6 \"%s/test-data/Path5\" has 0 data files, 1 content file, and 0 fallback archives. 0 files were replaced (0%%) (this is a duplicate path)", srcDir)
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	// expected = "Total data files found: 84"
	// if !strings.Contains(s, expected) {
	// 	t.Errorf("expected: %s", expected)
	// }

	expected = "Total fallback archives found: 2"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = "Total content files found: 4"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = "Total groundcover files found: 2"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	// expected = "Replaced file count: 56"
	// if !strings.Contains(s, expected) {
	// 	t.Errorf("expected: %s; got: %d", expected, actualReplacedCount)
	// }

	expected = " Bad content files were configured:"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = " Bad fallback archives were configured:"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = " Bad groundcover files were configured:"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = " Bad paths were configured:"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = " Duplicate content files were configured:"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = " Duplicate paths were configured:"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = " Empty paths were configured:"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = " Fully replaced paths were configured:"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}
}

func TestValidationsValidCheckPath(t *testing.T) {
	gc := game.NewGameConfig()
	var start time.Time
	var err error
	var d string
	tempDir := t.TempDir()
	out := filepath.Join(tempDir, "test.log")
	gc.Log = out
	d, err = os.Getwd()
	if err != nil {
		t.Error(err.Error())
	}
	srcDir := filepath.Join(d, "..")
	gc.CheckPath = fmt.Sprintf("%s/test-data/path1", srcDir)
	problemCfg := filepath.Join(srcDir, "valid-openmw.cfg")
	gc.Cfg = problemCfg
	file, err := os.OpenFile(out, os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		t.Error(err.Error())
	}
	defer file.Close()
	gc.Logger = log.New(file, false, true)
	gc.Greeting(false, gc.Cfg)
	gc, start, err = Run(gc)
	if err != nil {
		t.Error(err.Error())
	}
	Summary(gc, start)
	logOut, err := os.Open(out)
	if err != nil {
		t.Error(err.Error())
	}
	b, err := io.ReadAll(logOut)
	if err != nil {
		t.Error(err.Error())
	}
	s := string(b)

	expected := " ======== Welcome to openmw-validator"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = " ======== Performing validation:"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	// expected = fmt.Sprintf("Data path #1 \"%s/test-data/path1\" has 42 data files, 6 content files, and 2 fallback archives. 6 files were replaced", srcDir)
	// if !strings.Contains(s, expected) {
	// 	t.Errorf("expected: %s", expected)
	// }

	expected = fmt.Sprintf("Data path #2 \"%s/test-data/path2\" has 42 data files, 6 content files, and 2 fallback archives. 1 file was replaced", srcDir)
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = fmt.Sprintf("Data path #3 \"%s/test-data/path5\" has 0 data files, 1 content file, and 0 fallback archives. 0 files were replaced", srcDir)
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	// expected = "Total data files found: 84"
	// if !strings.Contains(s, expected) {
	// 	t.Errorf("expected: %s", expected)
	// }

	expected = "Total fallback archives found: 2"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = "Total content files found: 4"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = "Total groundcover files found: 2"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	// expected = "Replaced file count: 7"
	// if !strings.Contains(s, expected) {
	// 	t.Errorf("expected: %s", expected)
	// }

	expected = " ======== Reviewing path:"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	// expected = " data files found: 84"
	// if !strings.Contains(s, expected) {
	// 	t.Errorf("expected: %s", expected)
	// }

	expected = " Fallback archive count: 2 (4%)"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	expected = " Content file count: 6 (12%)"
	if !strings.Contains(s, expected) {
		t.Errorf("expected: %s", expected)
	}

	// expected = " Total Replaced file count: 7 (8%)"
	// if !strings.Contains(s, expected) {
	// 	t.Errorf("expected: %s", expected)
	// }
}
