package validations

import (
	"fmt"
	"os"
	"path/filepath"

	"git.sr.ht/~hristoast/openmw-validator/config"
	"git.sr.ht/~hristoast/openmw-validator/datafiles"
	"git.sr.ht/~hristoast/openmw-validator/game"
)

// Specific handling for data paths
func dataPaths(cfgData *config.CfgData, gameCfg *game.GameConfig) (*game.GameConfig, error) {
	gameCfg.Emit("Checking all data paths...")
	gameCfg.Emit("")

	order := 1
	for _, dp := range cfgData.Data {
		path := sanitizePath(dp, "data=")
		p := datafiles.NewPath(order, path)
		gameCfg.Emit(fmt.Sprintf("Checking: #%d %s", order, p.FullPath()))
		if p.Exists() {
			found := p.AlreadyFound(gameCfg.DataPaths, gameCfg.CaseSensitive)
			if found != nil {
				gameCfg.DuplicatePaths = append(gameCfg.DuplicatePaths, p.FullPath())
				gameCfg.Emit(fmt.Sprintf("WARNING: Duplicate path found: %s", p.FullPath()))
				path = path + "duplicate_data_path" + fmt.Sprintf("%d", order)
				found.Duplicate(true)
			}
			err := checkDataDirs(p, gameCfg, order)
			if err != nil {
				return gameCfg, err
			}
			gameCfg.DataPaths[path] = p
		} else {
			gameCfg.BadPaths = append(gameCfg.BadPaths, p.FullPath())
			gameCfg.Emit(fmt.Sprintf("WARNING: Bad path found: %s", p.FullPath()))
			continue
		}
		order += 1
	}
	return gameCfg, nil
}

// Recurse into a data path and scan all directories and files within it,
// registering info about each item into the GameConfig
func checkDataDirs(path *datafiles.Path, gameCfg *game.GameConfig, pathNum int) error {
	fileCount := 0
	pathContent, err := os.ReadDir(path.FullPath())
	if err != nil {
		return err
	}
	for _, pc := range pathContent {
		if pc.IsDir() {
			err = filepath.WalkDir(filepath.Join(path.FullPath(), pc.Name()),
				func(filePath string, d os.DirEntry, err error) error {
					if err != nil {
						return err
					}
					if !ignored(filePath) {
						if !d.IsDir() {
							// Register the file as a data file or already found
							fileCount = handleFile(filePath, path, gameCfg, fileCount, pathNum)
						}
					}
					return nil
				},
			)
			if err != nil {
				return err
			}

		} else {
			filePath := filepath.Join(path.FullPath(), pc.Name())
			if !ignored(filePath) {
				fileCount = handleFile(pc.Name(), path, gameCfg, fileCount, pathNum)
			}
		}
	}

	if fileCount == 0 {
		// No files were found that aren't ignored
		gameCfg.EmptyPaths = append(gameCfg.EmptyPaths, path.FullPath())
		gameCfg.Emit(fmt.Sprintf("WARNING: Empty path configured: %s", path.FullPath()))
	}
	return nil
}
