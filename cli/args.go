package cli

type args struct {
	CaseSensitive bool   `arg:"-C,--case-sensitive" help:"Make file and folder checks case sensitive"`
	CfgPath       string `arg:"-c,--cfg" help:"Path to an openmw.cfg file to validate" placeholder:"CFG-PATH"`
	CheckPath     string `arg:"-p,--check-path" help:"Report on an individual path, showing what is overwritten" placeholder:"DATA-PATH"`
	ExitNonZero   bool   `arg:"-E,--exit-nonzero" help:"Make the program exit non-zero if there's a problem (Linux/macOS only)"`
	NoReplace     bool   `arg:"-N,--no-replace" help:"Don't report when a data file is replaced by another"`
	Out           string `arg:"-o,--out" help:"Where to write the validation log to" placeholder:"LOG-PATH"`
	Quiet         bool   `arg:"-q,--quiet" help:"Only minimal logging output"`
	RootDrive     string `arg:"-D,--root-drive" help:"Indicate a different root drive (Windows only)"`
	Stdout        bool   `arg:"-s,--stdout" help:"Write the log to stdout instead of a file"`
	Verbose       bool   `arg:"--verbose,-v" help:"Print a lot of extra information about what the validator finds"`
}
