// Package cli is for running the validator cli
package cli

import (
	"fmt"
	"path/filepath"
	"strings"

	"git.sr.ht/~hristoast/openmw-validator/files"
	"git.sr.ht/~hristoast/openmw-validator/game"
	"git.sr.ht/~hristoast/openmw-validator/validations"
	"github.com/alexflint/go-arg"
)

func ParseArgs(userOS string) (*game.GameConfig, error) {
	g := game.NewGameConfig()
	cfgPath, logPath := files.DefaultCfgAndLog(userOS)
	g.Log = logPath

	var a args
	_ = arg.MustParse(&a)

	// Apply arg-provided values to the GameConfig
	if userOS != "windows" {
		g.CaseSensitive = a.CaseSensitive
	}
	g.CheckPath = a.CheckPath
	g.ExitNonZero = a.ExitNonZero
	g.NoReplace = a.NoReplace
	g.Quiet = a.Quiet
	g.Stdout = a.Stdout
	g.Verbose = a.Verbose

	if a.CfgPath == "" {
		g.Cfg = cfgPath
	} else {
		g.Cfg = a.CfgPath
	}

	p, err := filepath.Abs(g.Cfg)
	if err != nil {
		return nil, err
	}
	g.Cfg = p

	if !validations.CfgExists(g.Cfg) {
		return nil, fmt.Errorf("a cfg file could not be found at %s", g.Cfg)
	}

	if a.Out != "" {
		abs, err := filepath.Abs(a.Out)
		if err != nil {
			return nil, err
		}
		g.Log = abs
	}

	if a.RootDrive != "" {
		if strings.Contains(a.RootDrive, ":") {
			g.Cfg = strings.ReplaceAll(g.Cfg, "C:", a.RootDrive)
			g.Log = strings.ReplaceAll(g.Log, "C:", a.RootDrive)
		} else {
			g.Cfg = strings.ReplaceAll(g.Cfg, "C:", a.RootDrive+":")
			g.Log = strings.ReplaceAll(g.Log, "C:", a.RootDrive+":")
		}
	}

	return g, nil
}
