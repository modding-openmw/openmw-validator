## openmw-validator

Checks an `openmw.cfg` for:

* Errors such as typos on data paths, folders that don't exist, or similar mistakes
* A map of anything in your load order that overwrites files from another earlier load order entry - including a record of specific files that have been replaced
* Load order conflicts or problems that may be hard to notice in game or otherwise

Supports: Linux, macOS, and Windows.

#### Installation

Download OpenMW-Validator from [the project homepage](https://modding-openmw.gitlab.io/openmw-validator/).

##### Release Binary

Release binaries are available from the link above.

##### Source Build

Golang is required.

To build `openmw-validator`, ensure you have GNU Make installed and run one of the following commands depending on your OS and/or the build you desire:

```
make linux

make mac

make windows
```

The resulting binary will end up in the root of this repo. You can create a zip file with all binaries like this:

```
make
```

##### AUR

I've created [an AUR package](https://aur.archlinux.org/packages/openmw-validator/) for folks that wish to go that route.

#### Usage

```
Usage: openmw-validator [--case-sensitive] [--cfg CFG-PATH] [--check-path DATA-PATH] [--exit-nonzero] [--no-replace] [--out LOG-PATH] [--quiet] [--root-drive ROOT-DRIVE] [--stdout]

Options:
  --case-sensitive, -C   Make file and folder checks case sensitive
  --cfg CFG-PATH, -c CFG-PATH
                         Path to an openmw.cfg file to validate
  --check-path DATA-PATH, -p DATA-PATH
                         Report on an individual path, showing what is overwritten
  --exit-nonzero, -E     Make the program exit non-zero if there's a problem (Linux/macOS only)
  --no-replace, -N       Don't report when a data file is replaced by another
  --out LOG-PATH, -o LOG-PATH
                         Where to write the validation log to
  --quiet, -q            Only minimal logging output
  --root-drive ROOT-DRIVE, -D ROOT-DRIVE
                         Indicate a different root drive (Windows only)
  --stdout, -s           Write the log to stdout instead of a file
  --help, -h             display this help and exit
```

##### Check For Errors

Reports duplicated and empty data paths, as well as unusable content files.

```
openmw-validator --no-replace --quiet --stdout
```

##### Check An Alternate File

```
openmw-validator --cfg my-cool-openmw.cfg
```

##### Check A Specific Data Path

See what files from that path are overwitten by later paths.

```
openmw-validator --check-path c:\\games\\OpenMWMods\\MyCoolMod
```

##### Write the Validator Log To A Different Letter Drive (Windows)

Location `T:\Users\Todd\Documents\My Games\OpenMW\`:

```
openmw-validator --root-drive T
```

##### Write the Validator Log To A Different Location

Location `T:\Documents\My Games\Validator-log.txt`:

```
openmw-validator --out T:\Documents\My Games\Validator-log.txt
```
