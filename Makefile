proj_dir := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))

all: linux-amd64 linux-arm64 mac-amd64 mac-arm64 windows-amd64

linux-amd64 linux:
	./build.sh linux

linux-arm64:
	./build.sh linuxarm

mac-amd64 mac:
	./build.sh mac

mac-arm64:
	./build.sh macarm

windows-amd64 windows:
	./build.sh windows

podman-build-image:
	podman build -t openmw-validator:$$(git describe --tags) .

podman-build-openmw-validator:
	command podman run -v $(proj_dir):/mnt -it --rm openmw-validator:$$(git describe --tags)

podman-pkg: cleanall podman-build-image podman-build-openmw-validator

clean:
	rm -fr openmw-validator*.exe \
       openmw-validator-linux* openmw-validator-macos* \
       openmw-validator*/

cleanpkg:
	rm -f openmw-validator*.zip

cleancfg:
	rm -f problems-openmw.cfg valid-openmw.cfg

cleanall: clean cleancfg cleanpkg web-clean

rebuild: cleanall all

package pkg:
	./package.sh

problems-openmw.cfg:
	sed "s|_ROOT_PATH_|$(proj_dir)|g" problems-openmw.cfg.in > problems-openmw.cfg

valid-openmw.cfg:
	sed "s|_ROOT_PATH_|$(proj_dir)|g" valid-openmw.cfg.in > valid-openmw.cfg

cfg: problems-openmw.cfg valid-openmw.cfg

test: cleanall cfg
	GOPATH=$$HOME/.local/go go test -cover ./...

run-problems: cleancfg problems-openmw.cfg
	GOPATH=$$HOME/.local/go go run ./cmd/openmw-validator --cfg ./problems-openmw.cfg --stdout --exit-nonzero

run-problems-checkpath: cleancfg problems-openmw.cfg
	GOPATH=$$HOME/.local/go go run ./cmd/openmw-validator --cfg ./problems-openmw.cfg --stdout --exit-nonzero --check-path test-data/path1

run-valid: cleancfg valid-openmw.cfg
	GOPATH=$$HOME/.local/go go run ./cmd/openmw-validator --cfg ./valid-openmw.cfg --stdout --exit-nonzero

run-valid-checkpath: cleancfg valid-openmw.cfg
	GOPATH=$$HOME/.local/go go run ./cmd/openmw-validator --cfg ./valid-openmw.cfg --stdout --exit-nonzero --check-path ./test-data/path1

run: run-valid

run-linux-binary-problems: cleancfg problems-openmw.cfg linux
	./openmw-validator-linux-amd64 --cfg ./problems-openmw.cfg --stdout --exit-nonzero

run-linux-binary-valid: cleancfg valid-openmw.cfg linux
	./openmw-validator-linux-amd64 --cfg ./valid-openmw.cfg --stdout --exit-nonzero

run-linux-binary: run-linux-binary-problems


web-clean:
	cd web && rm -rf build site/*.md sha256sums soupault-*-linux-x86_64.tar.gz

web: web-clean
	cd web && ./build.sh

web-debug: web-clean
	cd web && ./build.sh --debug

web-verbose: web-clean
	cd web && ./build.sh --verbose

local-web:
	cd web && python3 -m http.server -d build
