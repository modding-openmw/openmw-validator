#!/bin/sh
set -e

this_dir=$(realpath "$(dirname "${0}")")
cd "${this_dir}"

mkdir openmw-validator
mv -v openmw-validator-*64* openmw-validator*.exe* openmw-validator/
zip -r openmw-validator.zip openmw-validator/
