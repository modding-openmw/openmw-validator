#!/bin/sh
set -e

export GOPATH="$HOME"/.local/go

target="${1-none}"

linux_amd64() {
    GOOS=linux GOARCH=amd64 go build -buildmode=pie -trimpath -mod=readonly -modcacherw -ldflags="-linkmode=external -s -w -X 'git.sr.ht/~hristoast/openmw-validator/config.Version=$(git describe --tags)'" -o ./openmw-validator-linux-amd64 ./cmd/openmw-validator
    sha256sum openmw-validator-linux-amd64 > openmw-validator-linux-amd64.sha256sum.txt
    ls -l ./openmw-validator-linux-amd64*
}

linux_arm64() {
    GOOS=linux GOARCH=arm64 go build -buildmode=pie -trimpath -mod=readonly -modcacherw -ldflags="-s -w -X 'git.sr.ht/~hristoast/openmw-validator/config.Version=$(git describe --tags)'" -o ./openmw-validator-linux-arm64 ./cmd/openmw-validator
    sha256sum openmw-validator-linux-arm64 > openmw-validator-linux-arm64.sha256sum.txt
    ls -l ./openmw-validator-linux-arm64*
}

mac_amd64() {
    GOOS=darwin GOARCH=amd64 go build -buildmode=pie -trimpath -mod=readonly -modcacherw -ldflags="-s -w -X 'git.sr.ht/~hristoast/openmw-validator/config.Version=$(git describe --tags)'" -o ./openmw-validator-macos-amd64 ./cmd/openmw-validator
    sha256sum openmw-validator-macos-amd64 > openmw-validator-macos-amd64.sha256sum.txt
    ls -l ./openmw-validator-macos-amd64*
}

mac_arm64() {
    GOOS=darwin GOARCH=arm64 go build -buildmode=pie -trimpath -mod=readonly -modcacherw -ldflags="-s -w -X 'git.sr.ht/~hristoast/openmw-validator/config.Version=$(git describe --tags)'" -o ./openmw-validator-macos-arm64 ./cmd/openmw-validator
    sha256sum openmw-validator-macos-arm64 > openmw-validator-macos-arm64.sha256sum.txt
    ls -l ./openmw-validator-macos-arm64*
}

windows_amd64() {
    GOOS=windows GOARCH=amd64 go build -buildmode=pie -trimpath -mod=readonly -modcacherw -ldflags="-s -w -X 'git.sr.ht/~hristoast/openmw-validator/config.Version=$(git describe --tags)'" -o ./openmw-validator.exe ./cmd/openmw-validator
    sha256sum openmw-validator.exe > openmw-validator.exe.sha256sum.txt
    ls -l ./openmw-validator.exe*
}

if [ "$target" = none ]; then
    linux_amd64
    linux_arm64
    mac_amd64
    mac_arm64
    windows_amd64

else
    if [ "$target" = "linux" ]; then
        linux_amd64
    elif [ "$target" = "linuxarm" ]; then
        linux_arm64
    elif [ "$target" = "mac" ]; then
        mac_amd64
    elif [ "$target" = "macarm" ]; then
        mac_arm64
    elif [ "$target" = "windows" ]; then
        windows_amd64
    fi
fi
