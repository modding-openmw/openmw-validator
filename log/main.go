// Package log provides a Logger object and several helper methods for logging messages.
package log

import (
	"fmt"
	"io"
	"log"
	"runtime"

	"git.sr.ht/~hristoast/openmw-validator/config"
)

type Logger struct {
	quiet   bool
	verbose bool
}

func New(out io.Writer, quiet, verbose bool) *Logger {
	log.SetOutput(out)
	return &Logger{quiet: quiet, verbose: verbose}
}

// Emit is a quiet-aware wrapper around log.Println
func (l *Logger) Emit(str string) {
	if !l.quiet && l.verbose {
		log.Println(str)
	}
}

// EmitError is a quiet-aware wrapper around log.Printf with a hardcoded prefix
func (l *Logger) EmitError(str string) {
	if !l.quiet && l.verbose {
		log.Printf("ERROR: %s", str)
	}
}

// Greeting prints the initial "hello" text for the application
// as well as other conditional information
func (l *Logger) Greeting(caseSensitive bool, cfg string) {
	if !l.quiet {
		log.Println(fmt.Sprintf("======== Welcome to openmw-validator v%s!", config.Version))
		log.Println("")
		log.Printf("Validating file: %s", cfg)
		if caseSensitive {
			log.Println("WARNING: Matches will be case sensitive!")
		} else {
			log.Println("Matches will not be case sensitive!")
		}
		l.Emit("")
		l.Emit("======== Performing validation:")
		l.Emit("")
	}
}

// EmitError is a wrapper around log.Printf with some formatting
func EmitError(str string) {
	log.Printf("ERROR: %s", str)
}

// Emit is a wrapper around log.Println
func Emit(str string) {
	log.Println(str)
	if runtime.GOOS == "windows" {
		// We want this to go to the console
		fmt.Println(str)
	}
}
