module git.sr.ht/~hristoast/openmw-validator

go 1.23.0

require (
	github.com/alexflint/go-arg v1.4.2
	golang.org/x/sys v0.26.0
)

require github.com/alexflint/go-scalar v1.0.0 // indirect
