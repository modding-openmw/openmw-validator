//go:build windows

package files

import "golang.org/x/sys/windows"

func windowsFolderPath() (string, error) {
	return windows.KnownFolderPath(windows.FOLDERID_Documents, 0)
}
