// Package files is for reading files.
package files

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"git.sr.ht/~hristoast/openmw-validator/config"
)

// DefaultCfgAndLog returns a string representing the default location of the `openmw.cfg` file.
// It should do the right thing on several OSes.
func DefaultCfgAndLog(userOS string) (string, string) {
	var cfg, log string
	rightNow := time.Now().Format("2006-01-02-150405")

	switch userOS {
	case "linux":
		cfg = fmt.Sprintf("%s/.config/openmw/openmw.cfg", os.Getenv("HOME"))
		log = fmt.Sprintf("%s/.config/openmw/validator-%s.log", os.Getenv("HOME"), rightNow)

	case "darwin":
		cfg = fmt.Sprintf("%s/Library/Preferences/openmw/openmw.cfg", os.Getenv("HOME"))
		log = fmt.Sprintf("%s/Library/Preferences/openmw/validator-%s.log", os.Getenv("HOME"), rightNow)

	case "windows":
		documents, err := windowsFolderPath()
		if err != nil {
			fmt.Println(err)
		}
		cfg = fmt.Sprintf("%s\\My Games\\OpenMW\\openmw.cfg", documents)
		log = fmt.Sprintf("%s\\My Games\\OpenMW\\validator-%s.log", documents, rightNow)

	default:
		// What OS is this?!
		cfg = fmt.Sprintf("%s/.config/openmw/openmw.cfg", os.Getenv("HOME"))
		log = fmt.Sprintf("%s/.config/openmw/validator-%s.log", os.Getenv("HOME"), rightNow)
	}
	return cfg, log
}

// ReadCfg gives back data that was read directly from the given CFG file.
// No validations or reading of any kind has been done beyond this.
func ReadCfg(filePath string) (*config.CfgData, error) {
	d := &config.CfgData{
		Content:     []string{},
		Groundcover: []string{},
		Data:        []string{},
	}

	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("the given cfg file does not exist: %s", filePath)
	}

	splitData := strings.Split(string(data), "\n")
	for _, line := range splitData {
		if strings.HasPrefix(line, "content=") {
			d.Content = append(d.Content, line)
		}
		if strings.HasPrefix(line, "data=") {
			d.Data = append(d.Data, line)
		}
		if strings.HasPrefix(line, "fallback-archive=") {
			d.FallbackArchives = append(d.FallbackArchives, line)
		}
		if strings.HasPrefix(line, "groundcover=") {
			d.Groundcover = append(d.Groundcover, line)
		}
	}

	return d, nil
}
