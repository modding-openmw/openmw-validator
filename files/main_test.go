package files

import (
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestFiles(t *testing.T) {
	d, err := os.Getwd()
	if err != nil {
		t.Error(err.Error())
	}
	problemCfg := filepath.Join(d, "..", "problems-openmw.cfg")
	cfgData, err := ReadCfg(problemCfg)
	if err != nil {
		t.Error(err.Error())
	}
	cfg, log := DefaultCfgAndLog(problemCfg)

	expected := "/openmw.cfg"
	if !strings.HasSuffix(cfg, expected) {
		t.Errorf("expected: ends with %s; got: %s", expected, cfg)
	}

	expected = ".log"
	if !strings.HasSuffix(log, expected) {
		t.Errorf("expected: ends with %s; got: %s", expected, log)
	}

	expected = "/validator-"
	if !strings.Contains(log, expected) {
		t.Errorf("expected: contains %s; got: %s", expected, log)
	}

	expected = "content=Test1.omwaddon"
	if cfgData.Content[3] != expected {
		t.Errorf("expected: %s; got: %s", expected, cfgData.Content[3])
	}

	expected = "fallback-archive=Test1.bsa"
	if cfgData.FallbackArchives[0] != expected {
		t.Errorf("expected: %s; got: %s", expected, cfgData.FallbackArchives[0])
	}

	expected = "groundcover=Groundcover1.omwaddon"
	if cfgData.Groundcover[0] != expected {
		t.Errorf("expected: %s; got: %s", expected, cfgData.Groundcover[0])
	}

	expected = "data=\""
	if !strings.Contains(cfgData.Data[0], expected) {
		t.Errorf("expected: contains %s; got: %s", expected, cfgData.Data[0])
	}

	expected = "test-data/path1"
	if !strings.Contains(cfgData.Data[0], expected) {
		t.Errorf("expected: contains %s; got: %s", expected, cfgData.Data[0])
	}
}
