FROM docker.io/library/debian:buster-slim

#
# See the project Makefile for the intended usage of this file.
#

ENV go_version 1.17.6

RUN apt-get update && apt-get install -y --force-yes build-essential ca-certificates git make wget zip

RUN wget https://golang.org/dl/go${go_version}.linux-amd64.tar.gz && tar -C /usr/local -xzf go${go_version}.linux-amd64.tar.gz && ln -s /usr/local/go/bin/go /usr/bin/

WORKDIR /mnt

CMD [ "make" ]
